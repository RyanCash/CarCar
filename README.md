# CarCar

Team:

* Miguel Ortiz - Service microservice
* Ray Cash - Sales microservice


## Design
## CarCar Diagram

![Img](/images/CarCar.png)


## How to Run this App
## Getting Started

Please make sure you have access to Docker Desktop application and your terminal.

## In your terminal
1. Change your directory to a folder you would  you would like to clone project to.
2. Run `git clone https://gitlab.com/miguemigue/project-beta.git`
3. Change your directory to the project folder `cd project-beta`
4. Setup docker(create volumes for the data, build images, build containers):

docker volume create beta-data
docker-compose build
docker-compose up

- When you run docker-compose up and if you're on macOS, you will see a warning about an environment variable named OS being missing. You can safely ignore this.
- Type in your browser: http://localhost:3000/
- You should now see the main page of the application with a navigation at the top of the page


## API Documentation

## Models for Inventory

1.Manufacturer
2.VehicleModel
3.Automobile



### URLs and Ports
1. Manufacturer:
| Action | Method | URL |
| ------------------------- | ------ | -------------------------------------------- |
| List Manufacturer | GET | http://localhost:8100/api/manufacturers/ |
| Specific Manufacturer | GET | http://localhost:8100/api/manufacturers/id/ |
| Create Manuacturer | POST | http://localhost:8100/api/manufacturers/ |
| Delete Maufacturer | DELETE | http://localhost:8100/api/manufacturers/id/ |
| Update Manufacturer | UPDATE | http://localhost:8100/api/manufacturers/id/ |


<details>
GET: Will return a list of manufacturers.

JSON request body:

      ```   
      {
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrystler"
		},
		{
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Chevrolet"
		},
		{
			"href": "/api/manufacturers/3/",
			"id": 3,
			"name": "Ford"
		}
	]
}
	```

<details>
<summary>Post: Will create new manufacturers  .</summary>

JSON request body:

      ```   
      {
  "name": "Ford"
    }

    ```

<details>
<summary>Get: Will get specific manufacturer with cooresponding id .</summary>
JSON request body:

      ```   
      {
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Chevrolet"
    }
    ```
<details>
<summary>DEL: Will delete manufacturer .</summary>
JSON request body:

      ```   
      {
	"id": null,
	"name": "Chevrolet 2"
    }

   ```

<details>
<summary>UPDATE : Will update a specific manufacturer with coorisponding id.</summary>
JSON request body:

	```  
{
	"href": "/api/manufacturers/3/",
	"id": 3,
	"name": "Chevrolet 2"
}


```

2.VehicleModel :
| Action | Method | URL |
| ------------------------- | ------ | -------------------------------------------- |
| List Model | GET | http://localhost:8100/api/models/ |
| Specific Model | GET | http://localhost:8100/api/models/id/ |
| Create Model | POST | http://localhost:8100/api/model/ |
| Delete Model| DELETE | http://localhost:8100/api/model/id/ |
| Update Model | UPDATE | http://localhost:8100/api/model/id/ |


<details>
<summary>GET: Will return a list of models.</summary>

JSON request body:
```

{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Sebring",
			"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Chrystler"
			}
		},
		{
			"href": "/api/models/2/",
			"id": 2,
			"name": "Camero",
			"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/2/",
				"id": 2,
				"name": "Chevrolet"
			}
		},
		{
			"href": "/api/models/3/",
			"id": 3,
			"name": "Mustang",
			"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/3/",
				"id": 3,
				"name": "Ford"
			}
		}
	]
}
       
	```

  

<details>
<summary>Post: Will create new models .</summary>

JSON request body:
```
{
  "name": "Mustang",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 3
}

{
	"href": "/api/models/3/",
	"id": 3,
	"name": "Mustang",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/3/",
		"id": 3,
		"name": "Ford"
	}
}
```



    

<details>
<summary>Get: Will get specific model with cooresponding id .</summary>
JSON request body:

      ```   
      {
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chrystler"
	}
}

      
```
<details>
<summary>DEL: Will delete model .</summary>
JSON request body:

      ```   
      {
	"id": null,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chrystler"
	}
    }
     
```
<details>
<summary>UPDATE : Will update a specific model with coorisponding id.</summary>
JSON request body:

      ```  
      {
  "name": "Silverado test",
  "picture_url": "https://springerfachmedien.azureedge.net/sfm-trucker/thumb_1200x675/media/5172/Chevrolet-Camaro-2016-01.jpg",
  "manufacturer_id": 2
}
{
  "name": "Silverado test 2",
  "picture_url": "https://springerfachmedien.azureedge.net/sfm-trucker/thumb_1200x675/media/5172/Chevrolet-Camaro-2016-01.jpg",
  "manufacturer_id": 2
}

```

3.Automobile :
| Action | Method | URL |
| ------------------------- | ------ | -------------------------------------------- |
| List Automobile | GET | http://localhost:8100/api/automobiles/ |
| Specific Automobile | GET | http://localhost:8100/api/automobiles/ |
| Create Automobile | POST | http://localhost:8100/api/automobiles/ |
| Delete Automobile| DELETE | http://localhost:8100/api/automobiles/ |
| Update Automobile | UPDATE | http://localhost:8100/api/automobiles/ |


<details>
<summary>GET: Will return a list of automobiles.</summary>

JSON request body:

      ```   
      {
	"autos": [
		{
			"href": "/api/automobiles/1C3CC5FB2AN125555/",
			"id": 2,
			"color": "red",
			"year": 2022,
			"vin": "1C3CC5FB2AN125555",
			"model": {
				"href": "/api/models/3/",
				"id": 3,
				"name": "Mustang",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/3/",
					"id": 3,
					"name": "Ford"
				}
			},
			"sold": false
		},
		{
			"href": "/api/automobiles/3vwd17ajxem428464/",
			"id": 3,
			"color": "rainbow",
			"year": 3,
			"vin": "3vwd17ajxem428464",
			"model": {
				"href": "/api/models/2/",
				"id": 2,
				"name": "Camero",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/2/",
					"id": 2,
					"name": "Chevrolet"
				}
			},
			"sold": false
		}
	]
}
```
<details>
<summary>Post: Will create new automobile.</summary>

JSON request body:

      ```   
      {
  "color": "red",
  "year": 2022,
  "vin": "1C3CC5FB2AN125555",
  "model_id": 3
    }
    ```
<details>
<summary>DEL: Will delete automobile.</summary>
JSON request body:

      ```   
      {
	"href": "/api/automobiles/1C3CC5FB2AN124444/",
	"id": null,
	"color": "white",
	"year": 2021,
	"vin": "1C3CC5FB2AN124444",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": false
}

<details>
<summary>UPDATE : Will update a specific automobile with coorisponding id.</summary>
JSON request body:

      ```  
      {
	"href": "/api/automobiles/1C3CC5FB2AN124444/",
	"id": 5,
	"color": "white",
	"year": 2021,
	"vin": "1C3CC5FB2AN124444",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": false
}
```

<details>
<summary>DEL: Will delete automobile.</summary>
JSON request body:

      ```  
       {
	"href": "/api/automobiles/1C3CC5FB2AN124444/",
	"id": 5,
	"color": "white",
	"year": 2021,
	"vin": "1C3CC5FB2AN124444",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": false
}
```
<details>
<summary>GET: Will return an automobile with coorespondind id.</summary>

JSON request body:

      ```   
    {
	"href": "/api/automobiles/1C3CC5FB2AN124444/",
	"id": 5,
	"color": "white",
	"year": 2021,
	"vin": "1C3CC5FB2AN124444",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": false
}
```



























​

​
​
### Service API
Using Insomnia, GET, POST and DELETE methods can be interacted with api endpoints.

| Action | Method | URL |
| ------------------------- | ------ | -------------------------------------------- |
| List Technicians | GET | http://localhost:8080/api/technicians/ |
| Create a technician | POST | http://localhost:8080/api/technicians/ |
| Delete a technician | DELETE | http://localhost:80*0/api/technicians/:id/ |
| List appointments | GET | http://localhost:8080/api/appointments/ |
| Delete appointment | DELETE | http://localhost:8080/api/appointments/:id |
| Set appointment to "canceled" | PUT | http://localhost:8080/api/appointments/:id/cancel |
| Set appointment to "finished" | PUT | http://localhost:8080/api/appointments/:id/finish |
​
**Technician endpoints**
POST Request: http://localhost:8080/api/technicians/ (BODY TYPE: JSON):

```
{
	"first_name": "Miguel",
	"last_name": "Ortiz",
	"employee_id": "2"
}
```
Return value:
```
{
	"first_name": "Miguel",
	"last_name": "Ortiz",
	"employee_id": "2",
	"id": 1
}
```

GET a list of Techncians return value:
```
{
	"technicians": [
		{
			"first_name": "Ryan",
			"last_name": "Cash",
			"employee_id": "111",
			"id": 1
		},
		{
			"first_name": "Josh",
			"last_name": "Elder",
			"employee_id": "222",
			"id": 2
		}
	]
}
```
**Appointment endpoints**
POST: http://localhost:8080/api/appointments/ (BODY: JSON):
```
{
	"vin": "NKJETE89WWE345E55",
	"date_time": "2023-06-09 07:30:00",
	"customer": "Bill Nye",
	"technician": "2",
	"reason": "Needs oil change",
	"status": "created"
}
```
Return value:
```
{
	"id": 1,
	"date_time": "2023-06-09 07:30:00",
	"reason": "Needs oil change",
	"vin": "NKJETE89WWE345E55",
	"technician": {
		"first_name": "Josh",
		"last_name": "Elder",
		"employee_id": "2",
		"id": 1
	},
	"customer": "Bill Nye",
	"status": "created"
}
```
GET a list of Appointments return value:
```
{
	"appointments": [
		{
			"id": 2,
			"date_time": "2023-07-04T08:15:00+00:00",
			"reason": "Missing tires",
			"vin": "H48SLWOSH38IR9453",
			"technician": {
				"first_name": "Mario",
				"last_name": "Bros",
				"employee_id": "10",
				"id": 4
			},
			"customer": "Lisa",
			"status": "finished"
		},
		{
			"id": 1,
			"date_time": "2023-07-05T08:20:00+00:00",
			"reason": "Missing gas pump",
			"vin": "JD84JDHEJDJDHFJDJ",
			"technician": {
				"first_name": "Luigi",
				"last_name": "Bros",
				"employee_id": "17",
				"id": 3
			},
			"customer": "Lupe",
			"status": "canceled"
		},
	]
}
```
PUT REQUESTS:
FINISH: PUT REQUEST TO: http://localhost:8080/api/appointments/2/finish/
This changes the Status field to "finished". You do not need to include a body.
CANCEL: PUT REQUEST TO: http://localhost:8080/api/appointments/3/cancel/
This changes the status field to "canceled". You do not need to include a body.

## Service microservice

There are three models: Technician, Appointment and AutomobileVO. While the docker container runs, the AutomobileVO is polled from the inventory api. Thru the front end, the VIP feature pulls data from the inventory api.

## Sales microservice

This microservice allows the user to manage the salespeople , customers , sales , and also gives the ability to view sales people history.

Salepeople customers and sales can be created and listed. 

Sales history is going to be a record of the sale that you can look up by the sales persons name.

## Models

1.AutomobileVO
2.Salesperson
3.Customer
4.Sale

The Sale model incorporates the AutomobileVo, Salesperson and Customer models through forein key relationships, maintaining a connection while preserving their individual definition.

### RESTful API calls

1. Salespeople:
| Action | Method | URL |
| ------------------------- | ------ | -------------------------------------------- |
| List Salespeople | GET | http://localhost:8090/api/salespeople/ |
| Create Salespeople | POST | http://localhost:8090/api/salespeople/ |
| Delete Salepeople | DELETE | http://localhost:8090/api/salespeople/id/ |

<details>
<summary>GET: Will return a list of all salespeople.</summary>

      JSON request body:

      ```   
	"salespeople": {
		{
			"id": 3,
			"first_name": "test",
			"last_name": "test",
			"employee_id": "1"
		},
		{
			"id": 4,
			"first_name": "test2",
			"last_name": "test"2,
			"employee_id": "2"
		},
      }
      ```

      


<details>
<summary>Post: Will create new sales people .</summary>

      JSON request body:



      ```
      
      {
	"first_name":"test3",
	"last_name":"test3",
	"employee_id":"3"
     }
      {
	"error": "Salesperson already exists"
    }
    ```




2. Customers 
| Action | Method | URL |
| ------------------------- | ------ | -------------------------------------------- |
| List customers | GET | http://localhost:8090/api/customers/ |
| Create customers | POST | http://localhost:8090/api/customers/ |
| Delete customers | DEL | http://localhost:8090/api/customers/id/ |

<details>
<summary>GET: Will return a list of all customers.</summary>

        JSON request body:
        ```
        {
	"customers": [
		{
			"id": 1,
			"first_name": "test",
			"last_name": "test",
			"address": "test",
			"phone_number": "123456780"
		},
		{
			"id": 3,
			"first_name": "Miguel",
			"last_name": "Ortiz",
			"address": "test",
			"phone_number": "123456780"
		}
	]
}
```
<details>
<summary>Post: Will create new customers .</summary>

JSON request body:
        ```
        {
	"first_name":"Miguel",
	"last_name":"Ortiz",
	"address":"test",
	"phone_number":"123456780"
}
```
3. Sale 
| Action | Method | URL |
| ------------------------- | ------ | -------------------------------------------- |
| List sale | Get | http://localhost:8090/api/sales/ |
| Create sale | POST | http://localhost:8090/api/sales/ |
| Delete sale | DEL | http://localhost:8090/api/sales/id/ |

<details>
<summary>GET: Will return a list of all sales.</summary>

  JSON request body:
        ```
        {
	"sales": [
		{
			"id": 2,
			"salesperson": 6,
			"automobile": "1C3CC5FB2AN124444",
			"customer": 1,
			"price": 10000
		},
		{
			"id": 3,
			"salesperson": 6,
			"automobile": "1C3CC5FB2AN125555",
			"customer": 1,
			"price": 15000
		}
	]
}
```
<details>
<summary>Post: Will create new sales .</summary>

    JSON request body:
            ```
     {
      "salesperson":3,
	  "customer":1,
	  "vin":"1C3CC5FB2AN125555",
	  "price": 15000
    }
    ```

## Value Objects
## Service Microservice Value Objects

The value object is the AutomobileVO model and is created from poller data from the inventory API. The inventory API stores the automobile instances. The Appointment model is another value object because of how it applies to time and an already created Technician can be assigned to an appointment. 

