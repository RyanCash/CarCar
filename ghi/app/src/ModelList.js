import React,{useState,useEffect} from 'react';

function VehicleModelForm() {
    const [vehicles, setVehicles] = useState([]);

    const fetchVehicles = async () => {
        const Vehicleurl = "http://localhost:8100/api/models/"
        const response = await fetch(Vehicleurl);
        if (response.ok) {
            const data = await response.json();
            setVehicles(data.models);

        }
    }
    useEffect(() => {
        fetchVehicles();
    }, []);

    return (
        <>
        <h1>Vehicle Models</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Picture</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    </tr>
            </thead>
            <tbody>
                {vehicles.map((vehicle) => {
                    return (
                    <tr key={vehicle.href}>
                        <td>{vehicle.name}</td>
                        <td>{vehicle.manufacturer.name}</td>
                        <td className="w-25">
                            <img src={vehicle.picture_url} className="img-thumbnail img"></img>
                            </td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    );
}

export default VehicleModelForm;


        
