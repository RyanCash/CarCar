import { useEffect, useState } from "react";

function ListAppointments() {
    const [appointments, setAppointments] = useState([]);
    const [autos, setAutos] = useState([]);

    const fetchAutos = async () => {
        const response = await fetch("http://localhost:8100/api/automobiles");
    if (response.ok) {
        const data = await response.json();
        setAutos(data.autos);
    } else {
        console.error(response);
    }
    }

    const fetchAppointments = async () => {
        const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
        const data = await response.json();
        const createdAppointments = data.appointments.filter(appointment => appointment.status==="created")
        setAppointments(createdAppointments);
    } else {
        console.error(response);
    }
    }

    useEffect(() => {
        fetchAppointments();
        fetchAutos();
    }, []);

    const handleFinished = async (id) => {
        const statusUrl = `http://localhost:8080/api/appointments/${id}/finish/`;
        const fetchOptions = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
        };

        const finishResponse = await fetch(statusUrl, fetchOptions);
        if (finishResponse.ok) {
            console.log("message: sucessfully marked as finished")
            fetchAppointments();
        }}

        const handleCanceled = async (id) => {
            const statusUrl = `http://localhost:8080/api/appointments/${id}/cancel/`;
            const fetchOptions = {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                },
            };

            const finishResponse = await fetch(statusUrl, fetchOptions);
            if (finishResponse.ok) {
                console.log("message: successfully marked as canceled")
                fetchAppointments();
            }}

function VIP(vin, autosList) {
    let vinList = [];

    for (const auto of autosList) {
        vinList.push(auto.vin)}

    if (vinList.includes(vin)) {
        return <td>Yes</td>;
    } else {
        return <td>No</td>;
    }
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th> </th>
                    <th> </th>
                </tr>
            </thead>
            <tbody>
                {appointments.map(appointment => {
                    const newDate = new Date(appointment.date_time)
                    const options = {
                        day: "numeric",
                        month: "numeric",
                        year: "numeric",
                    };
                    return(<tr key={appointment.id}>
                        <td>{appointment.vin}</td>
                            {VIP(appointment.vin, autos)}
                        <td>{appointment.customer}</td>
                        <td>{newDate.toLocaleString('en-US', options)}</td>
                        <td>{newDate.toLocaleTimeString('en-US')}</td>
                        <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                        <td>{appointment.reason}</td>
                        <td><button name="finishButton" onClick={() => handleFinished(`${appointment.id}`)}>Finish</button></td>
                        <td><button name="cancelButton" onClick={() => handleCanceled(`${appointment.id}`)}>Cancel</button></td>
                    </tr>
                    )
                })}
            </tbody>
        </table>
    )
}

export default ListAppointments;