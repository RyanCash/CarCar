import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/">Manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/form/">Create a Manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/vehicles/">Vehicle Models</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/vehicles/create">Create Vehicle Model</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/automobiles/">Automobiles</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/form/">Create an Automobile</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/salespeople/">Salespeople</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/salespeople/create">Create Salespeople</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/customers/">Customers</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/sales/">Sales</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/sales/create/">Add a Sale</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/technicians/">List of Technicians</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link" to="/technicians/create">Add a Technician</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/appointments/">Appointment List</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/appointments/create/">Create a Service Appointment</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/appointments/history/">Appointment History</NavLink>
              </li>
              <li>
                <NavLink className="nav-link" to="/salespeople/history/">Salesperson History</NavLink>
              </li>   
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
