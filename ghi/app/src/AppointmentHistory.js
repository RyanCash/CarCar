import { useEffect, useState } from "react";

function AppointmentHistory() {
    const [appointments, setAppointments] = useState([]);
    const [autos, setAutos] = useState([]);
    const [search, setSearch] = useState(['']);

    const handleSearchChange = (event) => {
        const value = event.target.value;
        setSearch(value);
    }

    const fetchAutos = async () => {
        const response = await fetch("http://localhost:8100/api/automobiles");
    if (response.ok) {
        const data = await response.json();
        setAutos(data.autos);
    } else {
        console.error(response);
    }
    }

    const fetchAppointments = async () => {
        const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments);
    } else {
        console.error(response);
    }
    }

    useEffect(() => {
        fetchAppointments();
        fetchAutos();
    }, []);

function VIP(vin, autosList) {
    let vinList = [];
    for (const auto of autosList) {
        vinList.push(auto.vin);
    }
    if (vinList.includes(vin)) {
        return <td>Yes</td>;
    } else {
        return <td>No</td>;
    }
    }

    return(
        <div>
            <h1 className="text-center mb-4">Service History</h1>
            <input type="search" onChange={handleSearchChange} value={search} placeholder="Search by VIN" />
                <button className="btn btn-primary">Search</button>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.filter(appointment=>appointment.vin.toLowerCase().startsWith(search.toLowerCase()).map(appointment => {
                        const newDate = new Date(appointment.date_time)
                        const options = {
                          day: "numeric",
                          month: "numeric",
                          year: "numeric",
                        };
                        return(<tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                                {VIP(appointment.vin, autos)}
                            <td>{appointment.customer}</td>
                            <td>{newDate.toLocaleString('en-US', options)}</td>
                            <td>{newDate.toLocaleTimeString('en-US')}</td>
                            <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.status}</td>
                        </tr>)
                    }))}
                </tbody>
            </table>
        </div>
    ) 
}

export default AppointmentHistory;