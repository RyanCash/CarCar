import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from './ManufacturerList';
import CreateManufacturer from './CreateManufacturer';
import CreateAutomobile from './CreateAutomobile';
import NewModelForm from './NewModelForm';
import ModelList from './ModelList';
import AutomobileList from './AutomobileList';
import CreateTechnician from './CreateTechnician';
import NewSalespersonForm from './NewSalespersonForm';
import SalespersonList from './SalespeopleList';
import NewCustomerForm from './NewCustomerForm';
import ListTechnicians from './ListTechnicians';
import CreateServiceAppointment from './CreateServiceAppointment';
import ListAppointments from './ListAppointments';
import AppointmentHistory from './AppointmentHistory';
import RecordNewSale from './RecordNewSale';
import SalesList from './SalesList';
import CustomerList from './CustomerList';
import SalespersonHistory from './SalespersonHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/ModelList" element={<ModelList />} />
          <Route path="/NewModelForm" element={<NewModelForm />} />
          <Route path="/vehicles" element={<ModelList />} />
          <Route path="/vehicles/create" element={<NewModelForm />} />
          <Route path="/AutomobileList" element={<AutomobileList />} />
          <Route path="/automobiles" element={<AutomobileList />} />
          <Route path="/manufacturers/" element={<ManufacturerList />} />
          <Route path="/manufacturers/form/" element={<CreateManufacturer />} />
          <Route path="/automobiles/form" element={<CreateAutomobile />} />
          <Route path="/technicians/create" element={<CreateTechnician />} />
          <Route path="/salespeople/create" element={<NewSalespersonForm/>} />
          <Route path="/salespeople" element={<SalespersonList/>} />
          <Route path="/customers/create" element={<NewCustomerForm/>} />
          <Route path="/technicians/" element={<ListTechnicians/>} />
          <Route path="/appointments/create/" element={<CreateServiceAppointment/>} />
          <Route path="/appointments/" element={<ListAppointments/>} />
          <Route path="/appointments/history/" element={<AppointmentHistory/>} />
          <Route path="/sales" element={<SalesList/>} />
          <Route path="/sales/create" element={<RecordNewSale/>} />
          <Route path="/customers/" element={<CustomerList/>}/>
          <Route path="/salespeople/history/" element={<SalespersonHistory/>}/>
          <Route path="/sales/create/" element={<RecordNewSale/>} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
