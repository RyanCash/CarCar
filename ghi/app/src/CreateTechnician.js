import { useState } from "react";

function CreateTechnicianForm() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }
    const handleEmployeeIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name=firstName;
        data.last_name=lastName;
        data.employee_id=employeeId;
        const technicianUrl = 'http://localhost:8080/api/technicians/'
        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json'},
        }
    

        const technicianResponse = await fetch(technicianUrl, fetchOptions);
        if (technicianResponse.ok) {
            setFirstName('');
            setLastName('');
            setEmployeeId('');
            const newTechnician = await technicianResponse.json();
            console.log(newTechnician);
        }
    }

    return(      
        <div className="my-5 container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a Technician</h1>
                        <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input onChange={handleFirstNameChange} required placeholder="Enter first name here" name="firstName" id="firstName" value={firstName} />
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLastNameChange} required placeholder="Enter last name here" name="lastName" id="lastName" value={lastName} />
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployeeIdChange} required placeholder="Enter employee ID  here" name="employeeId" id="employeeId" value={employeeId} />
                        </div>
                        <div className="form-floating mb-3">
                            <button className="btn btn-primary">Create</button>
                        </div>
                        </form>
                    </div>
                </div>    
            </div>
        </div>
    )

}

export default CreateTechnicianForm;