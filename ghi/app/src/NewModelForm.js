import React,{useState, useEffect} from "react";
import { useNavigate } from "react-router-dom";


function NewModelForm() {
    const [manufacturers, setManufacturers] = useState([]);
    const [name, setName] = useState("");
    const [manufacturerId, setManufacturerId] = useState("");
    
    const [pictureUrl, setPictureUrl] = useState("");
    const navigate = useNavigate();

    const fetchManufacturers = async () => {
        
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        
    
    }
    };



    useEffect(() => {
        fetchManufacturers();
    }, []);
    

const handleSubmit = async (event) => {
    event.preventDefault();
    const  data = {};
    data.name = name;
    data.manufacturerId = manufacturerId;
    data.pictureUrl = pictureUrl;


    const modelUrl = "http://localhost:8100/api/models/"; 
    const fetchOptions = {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    };
    const response = await fetch(modelUrl, fetchOptions);


  
    
    if (response.ok) {
        setName("");
        setManufacturerId("");
        setPictureUrl("");
        navigate("/models");
    }
};

return (
    <div className="my-5 container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                    <div className="card-body">
                        <h2 className="card-title">Create Vehicle Model</h2>
                        <form onSubmit={handleSubmit}>
                            <div className="form-group mb-3">
                                <input onChange={(event) => setName(event.target.value)} value={name} type="text" className="form-control" placeholder="Model name" />
                            </div>
                            <div className="form-group mb-3">
                                <input onChange={(event) =>setPictureUrl(event.target.value)} value={pictureUrl} type="text" className="form-control" placeholder="Picture URL" />
                            </div>
                            <div className="form-group mb-3">
                                <select onChange={(event) => setManufacturerId(event.target.value)} value={manufacturerId} id="manufacturerId" className="form-select">
                                    <option value="">Select a manufacturer</option>
                                    {manufacturers.map((manufacturer) => {
                                        return (
                                        <option key={manufacturer.id} value={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button type="submit" className="btn btn-primary">Create Vehicle</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
);
}

export default NewModelForm;