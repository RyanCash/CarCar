import React, {useState, useEffect} from 'react';
import { useNavigate } from 'react-router-dom';

function NewSalespersonForm() {
    const [ firstName, setFirstName ] = useState("");
    const [ lastName, setLastName ] = useState("");
    const [employeeId, setEmployeeId] = useState("");

    const navigate = useNavigate();

    const handleFirstNameChange = async (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = async (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleEmployeeIdChange = async (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }

    
    
    
    const salespersonUrl = "http://localhost:8090/api/salespeople/";
    
    
    
    const handleSubmit = async (event) => {
        event.preventDefault();
        
        const data = {
            first_name: firstName,
            last_name: lastName,
            employee_id: employeeId,
        }

        const fetchConfig = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
        };

            const response = await fetch(salespersonUrl, fetchConfig);
            if(response.ok){
                setFirstName("");
                setLastName("");
                setEmployeeId("");
                navigate("/salespeople");
                const newSalesperson = await response.json();
                console.log(newSalesperson);
            }
    }

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1> Create a Salesperson</h1>
                        <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input
                            value={firstName}
                            onChange={handleFirstNameChange}
                            required
                            type="text"
                            className="form-control"
                            id="name"
                            placeholder="First Name"
                            />
                            <label htmlFor="name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                            value={lastName}
                            onChange={handleLastNameChange}
                            placeholder='Last Name'
                            required
                            type="text"
                            name="lastName"
                            id="lastName"
                            className="form-control"
                            />
                            <label htmlFor="lastName">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                            value={employeeId}
                            onChange={handleEmployeeIdChange}
                            placeholder='Employee Id'
                            required
                            type="text"
                            name="employee id"
                            id="employee id"
                            className="form-control"
                            />
                            <label htmlFor="employee id">Employee Id</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Create Salesperson</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default NewSalespersonForm;