import React, {useState,useEffect} from "react";
import { useNavigate } from "react-router-dom";

function NewCustomerForm() {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [address, setAddress] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");

    const navigate = useNavigate();

    const handleFirstNameChange = async (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = async (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleAddressChange = async (event) => {
        const value = event.target.value;
        setAddress(value);
    }

    const handlePhoneNumberChange = async (event) => {
        const value = event.target.value;
        setPhoneNumber(value);
    }

    const customerUrl = "http://localhost:8090/api/customers/";

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            first_name: firstName,
            last_name: lastName,
            address: address,
            phone_number: phoneNumber,
        }

        const fetchConfig = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
        };

            const response = await fetch(customerUrl, fetchConfig);
            if(response.ok){
                setFirstName("");
                setLastName("");
                setAddress("");
                setPhoneNumber("");
                navigate("/customers");
                const newCustomer = await response.json();
                console.log(newCustomer);
            }
    }

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1 className="text-center mb-4">New Customer</h1>
                        <form onSubmit={handleSubmit} id="create-customer-form">
                        <div className="form-floating mb-3">
                            <input
                            value={firstName}
                            onChange={handleFirstNameChange}
                            type="text"
                            className="form-control"
                            required
                            placeholder="First Name"
                            id="firstName"
                            name="firstName"
                            />
                            <label htmlFor="name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                            value={lastName}
                            onChange={handleLastNameChange}
                            type="text"
                            className="form-control"
                            required
                            placeholder="Last Name"
                            id="lastName"
                            name="lastName"
                            />
                            <label htmlFor="name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                            value={address}
                            onChange={handleAddressChange}
                            type="text"
                            className="form-control"
                            required
                            placeholder="Address"
                            id="address"
                            name="address"
                            />
                            <label htmlFor="name">Address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                            value={phoneNumber}
                            onChange={handlePhoneNumberChange}
                            type="text"
                            className="form-control"
                            required
                            placeholder="Phone Number"
                            id="phoneNumber"
                            name="phoneNumber"
                            />
                            <label htmlFor="name">Phone Number</label>
                        </div>
                            <button type="submit" className="btn btn-success btn-lg">Create Customer</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default NewCustomerForm;



