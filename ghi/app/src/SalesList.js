import React,{useState,useEffect} from "react";


function SalesListForm() {
    const [sales, setSales] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8090/api/sales/"
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
        <h1>Sales</h1>
        <table className="table table-image">
            <thead>
                <tr>
                    <th>Automobile</th>
                    <th>Salesperson</th>
                    <th>Customer</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {sales.map((sale) => {
                    return (
                    <tr key={sale.id}>
                        <td>{sale.automobile.vin}</td>
                        <td>{sale.salesperson.employee_id}</td>
                        <td>{sale.customer.first_name}</td>
                        <td>{sale.price}</td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    )
}

export default SalesListForm;