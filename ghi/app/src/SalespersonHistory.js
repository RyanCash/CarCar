import React, {useState,useEffect} from 'react';

function SalespersonHistory() {
    const [sales, setSales] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [salesData, setSalesData] = useState([]);

    const fetchSalesData = async () => {
        const response = await fetch("http://localhost:8090/api/sales/");
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
            setSalesData(data.sales);
        }
        else {
            console.error(response);
        }
    }

    const fetchSalespeopleData = async () => {
        const response = await fetch("http://localhost:8090/api/salespeople/");
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
        else {
            console.error(response);
        }
    }

    useEffect(() => {
        fetchSalespeopleData();
        fetchSalesData();
    }
    , []);

    const handleSalesPersonSelect = (event) => {
        const value = event.target.value;
        var updatedSales
        if (value === "all") {
            updatedSales = salesData
        }
        else {
            updatedSales = salesData.filter((sale) => sale.salesperson.id === parseFloat(value))
        }
        setSales(updatedSales)
    }

    return (
        <>
        <h1>Salesperson History</h1>
        <select onChange={handleSalesPersonSelect} required id="salesperson" name="salesperson" className="salesperson-select">
            <option value={"all"} key={"all"}>All</option>
            {salespeople.map((salesperson) => {
                return (
                    <option value={salesperson.id} key={salesperson.id}>
                        {salesperson.first_name} {salesperson.last_name}
                    </option>
                )
            }
            )}
        </select>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Salesperson</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {sales.map((sale) => {
                    return (
                        <tr key={sale.id}>
                            <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </>
    )
}

export default SalespersonHistory;