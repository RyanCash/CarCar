import { useEffect, useState } from "react";

function CreateAutomobileForm() {
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [models, setModels] = useState([]);
    const [model, setModel] = useState('');

    const handleChange = (event, callback) => {
        const {target} = event;
        const {value} = target;
        callback(value);
    }

    const fetchData = async() => {
        const response = await fetch("http://localhost:8100/api/models/");

        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
            console.log(data.models);
        } else {
            console.error(response);
        }
    }
    useEffect(() => {
        fetchData();
    }, [])

    const handleSubmit = async(event) => {
        event.preventDefault();

        const data = {};
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model;

        const url = "http://localhost:8100/api/automobiles/";
        const json = JSON.stringify(data);
        const fetchConfig = {
            method: "POST", 
            body: json, 
            headers: {"Content-Type": "application/json"}
        }

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            const newAutomobile = await response.json();
            console.log(newAutomobile);

            setColor('');
            setYear('');
            setVin('');
            setModel('');
        } else {
            console.error(response);
        }
    };

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add an automobile to the inventory</h1>
                        <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input onChange={(event) => {handleChange(event, setColor)}} value={color} required placeholder="Color..." name="color" id="color" />
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(event) => {handleChange(event, setYear)}} type='number' value={year} required placeholder="Year..." name="year" id="year" />
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(event) => {handleChange(event, setVin)}} value={vin} required placeholder="VIN..." name="VIN" id="VIN" />
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={(event) => {handleChange(event, setModel)}} value={model} required name="model" id="model" className='form-select'>
                                <option>Choose a model...</option>
                                    {models.map((model) => {
                                        return (
                                            <option key={model.id} value={model.id}>
                                                {model.name}
                                            </option>
                                        )
                                    })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <button className="btn btn-primary">Create</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CreateAutomobileForm;