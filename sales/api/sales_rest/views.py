from django.shortcuts import render, get_object_or_404
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
# Create your views here.
from .encoders import SalespersonEncoder, SaleEncoder, CustomerEncoder, AutomobileVOEncoder
from .models import Salesperson, Sale, Customer, AutomobileVO


@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    if request.method == "GET":
        salesperson = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": list(salesperson.values())},
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            sdr = content["employee_id"]
            if Salesperson.objects.filter(employee_id=sdr).exists():
                return JsonResponse(
                    {"error": "Salesperson already exists"},
                    status=400,
                )
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                    {"salesperson": SalespersonEncoder().default(salesperson)},
                    safe=False,
                )
        except KeyError:
            return JsonResponse(
                {"error": "No employee_id data in request body"},
                status=400,
            )


@require_http_methods(["DELETE"])
def delete_salespeople(request, id):
    if request.method == "DELETE":
        if not Salesperson.objects.filter(id=id).exists():
            return JsonResponse({"error": "Salesperson does not exist"}, status=404)
        count, _ = Salesperson.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0},
            )


@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customers": list(customer.values())},
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            {"customer": CustomerEncoder().default(customer)},
        )

@require_http_methods(["DELETE"])
def delete_customers(request, id):
    if request.method == "DELETE":
        if not Customer.objects.filter(id=id).exists():
            return JsonResponse({"error": "Customer does not exist"}, status=404)
        count, _ = Customer.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0},
            )

@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        sales_data = [SaleEncoder().default(sale) for sale in sales]
        return JsonResponse(
            {"sales": sales_data},
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(vin=content["vin"])
            salesperson = Salesperson.objects.get(employee_id=content["salesperson"])
            customer = get_object_or_404(Customer, id=content["customer"])
            sale = Sale.objects.create(
                automobile=automobile,
                salesperson=salesperson,
                customer=customer,       
                price=content["price"],
            )
            sale_dict = {
                "automobile": sale.automobile.vin,
                "salesperson": sale.salesperson.employee_id,
                "customer": sale.customer.id,
                "price": sale.price,
            }
            return JsonResponse(
                sale_dict,
                safe=False,
            )
        except KeyError:
            return JsonResponse(
                {"error": "vin, salesperson, or customer data not found in request body"},
                status=400,
            )
        except (AutomobileVO.DoesNotExist, Salesperson.DoesNotExist, Customer.DoesNotExist):
            return JsonResponse(
                    {"error": "Automobile, Salesperson, or Customer does not exist"},
                    status=400,
                )


@require_http_methods(["DELETE"])
def delete_sales(request, id):
    if request.method == "DELETE":
        if not Sale.objects.filter(id=id).exists():
            return JsonResponse({"error": "Sale does not exist"}, status=404)
        count, _ = Sale.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0},
            )
