from common.json import ModelEncoder
from .models import Salesperson, Sale, Customer, AutomobileVO


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]


class SaleEncoder(ModelEncoder):
    def default(self, obj):
        if isinstance(obj, Sale):
            return {
                'id': obj.id,
                'salesperson': obj.salesperson.id,
                'automobile': obj.automobile.vin,
                'customer': obj.customer.id,
                'price': obj.price
            }
        return super().default(obj)
    encoder = {
        "automobile": AutomobileVO,
        "salesperson": SalespersonEncoder,
        "customer": CustomerEncoder,
    }


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]