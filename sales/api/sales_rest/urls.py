from django.urls import path
from .views import (
    list_salespeople,
    list_customers,
    list_sales,
    delete_salespeople,
    delete_customers,
    delete_sales
)


urlpatterns = [
    path("salespeople/", list_salespeople, name="list_salespeople"),

    path("customers/", list_customers, name="list_customers"),
    path("sales/", list_sales, name="list_sales"),
    path("salespeople/<int:id>/", delete_salespeople, name="delete_salespeople"),
    path("customers/<int:id>/", delete_customers, name="delete_customers"),
    path("sales/<int:id>/", delete_sales, name="delete_sales"),
]
