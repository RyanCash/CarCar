from django.urls import path

from .views import (
    api_technicians,
    api_technician,
    api_appointments,
    api_delete_appointment,
    api_finish_appointment,
    api_cancel_appointment,
)

urlpatterns = [
    path("technicians/", api_technicians, name="api_technicians"),
    path("technicians/<int:pk>/", api_technician, name="api_manufacturer"),
    path("appointments/", api_appointments, name="api_appointments"),
    path("appointments/<int:pk>", api_delete_appointment, name="delete_appointment"),
    path("appointments/<int:pk>/finish/", api_finish_appointment, name="finish_appointment"),
    path("appointments/<int:pk>/cancel/", api_cancel_appointment, name="cancel_appointment"),
]